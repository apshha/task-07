package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private Connection connection;
    private static final Lock CONNECTION_LOCK = new ReentrantLock();
    private static final String SQL_INSERT_USER = "INSERT INTO users VALUES (DEFAULT ,?)";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT ,?)";
    private static final String SQL_INSERT_USER_TO_TEAM = "INSERT INTO users_teams VALUES(?,?)";
    private static final String SQL_DELETE_USERS = "DELETE FROM users WHERE id=?";


    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            try {
                instance = new DBManager();
            } catch (IOException | SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private DBManager() throws IOException, SQLException {
        Properties properties = new Properties();
        FileInputStream fis;
        try {
            fis = new FileInputStream("app.properties");
            properties.load(fis);
            String connectionUrl = properties.getProperty("connection.url");
            connection = DriverManager.getConnection(connectionUrl);
        } catch (IOException e) {
            System.err.println("Priorities don't exist");
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> listOfUsers = new ArrayList<>();
        String query = "SELECT * FROM users";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            CONNECTION_LOCK.lock();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                listOfUsers.add(user);
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(stmt);
            close(rs);
            CONNECTION_LOCK.unlock();
        }
        return listOfUsers;
    }

    public boolean insertUser(User user) throws DBException {

            PreparedStatement ps = null;
            ResultSet id = null;

            try {
                CONNECTION_LOCK.lock();
                ps = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, user.getLogin());
                if (ps.executeUpdate() != 1) {
                    return false;
                }
                id = ps.getGeneratedKeys();
                if (id.next()) {
                    int idField = id.getInt(1);
                    user.setId(idField);
                }
            } catch (SQLException ex) {
               return false;
            } finally {
                close(id);
                close(ps);
                CONNECTION_LOCK.unlock();
            }
            return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(SQL_INSERT_USER_TO_TEAM);
            for (User user : users) {
                ps.setInt(1, user.getId());
                ps.addBatch();
            }
            int[] usersResult = ps.executeBatch();
            for (int i : usersResult) {
                if (i != 1) {
                    return false;
                }
            }
            connection.commit();
            return true;
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        } finally {
            close(rs);
            close(ps);
            setAutocommit();
            CONNECTION_LOCK.unlock();
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        String query = "SELECT * FROM users WHERE login='" + login + "'";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            CONNECTION_LOCK.lock();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(stmt);
            close(rs);
            CONNECTION_LOCK.unlock();
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        String query = "SELECT * FROM teams WHERE name='" + name + "'";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            CONNECTION_LOCK.lock();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(stmt);
            close(rs);
            CONNECTION_LOCK.unlock();
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> listOfTeam = new ArrayList<>();
        String query = "SELECT * FROM teams";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            CONNECTION_LOCK.lock();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt(1));
                team.setName(rs.getString(2));
                listOfTeam.add(team);
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(stmt);
            close(rs);
            CONNECTION_LOCK.unlock();
        }
        return listOfTeam;
    }

    public boolean insertTeam(Team team) throws DBException {
        PreparedStatement ps = null;
        ResultSet id = null;

        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, team.getName());
            if (ps.executeUpdate() != 1) {
                return false;
            }
            id = ps.getGeneratedKeys();
            if (id.next()) {
                int idField = id.getInt(1);
                team.setId(idField);
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(id);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(SQL_INSERT_USER_TO_TEAM);
            for (Team t : teams) {
                ps.setInt(1, user.getId());
                ps.setInt(2, t.getId());
                ps.addBatch();
            }
            int[] usersTeams = ps.executeBatch();
            for (int i : usersTeams) {
                if (i != 1) {
                   throw new SQLTransientException("Transaction failed");
                }
            }
            connection.commit();
            return true;
        } catch (SQLException ex) {
            try {
                connection.rollback();
                throw new DBException(ex.getMessage(), ex);
            } catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        } finally {
            close(rs);
            close(ps);
            setAutocommit();
            CONNECTION_LOCK.unlock();
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> listOfTeam = new ArrayList<>();
        String query = "SELECT * FROM users_teams left OUTER JOIN teams ON team_id=id WHERE user_id=" + user.getId();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            CONNECTION_LOCK.lock();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt(3));
                team.setName(rs.getString(4));
                listOfTeam.add(team);
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(stmt);
            close(rs);
            CONNECTION_LOCK.unlock();
        }
        return listOfTeam;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String query = "DELETE FROM teams WHERE id=" + team.getId();
        Statement stmt = null;
        try {
            CONNECTION_LOCK.lock();
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(stmt);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        String query = "UPDATE teams SET name = '" + team.getName() + "' WHERE id =" + team.getId();
        Statement stmt = null;
        try {
            CONNECTION_LOCK.lock();
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(stmt);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    private static void close(Statement st) throws DBException {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException ex) {
                throw new DBException(ex.getMessage(), ex);
            }
        }
    }

    private static void close(ResultSet rs) throws DBException {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                throw new DBException(ex.getMessage(), ex);
            }
        }
    }

    private void setAutocommit() {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
